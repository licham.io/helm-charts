{{- if .Values.networkShare.enabled }}

apiVersion: v1
kind: Service
metadata:
  name: {{ include "licham-application.fullname" . }}-network-share
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "licham-application.labels" . | nindent 4 }}
  annotations:
    external-dns.alpha.kubernetes.io/hostname: {{ .Values.networkShare.hostname }}
spec:
  type: LoadBalancer
  ports:
    - port: 445
      targetPort: 445
      protocol: TCP
      name: samba
  selector:
    app: network-share

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "licham-application.fullname" . }}-network-share
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "licham-application.labels" . | nindent 4 }}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: network-share
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: network-share
        topology.k8s.licham.io/spread: storage
    spec:
      serviceAccountName: {{ include "licham-application.serviceAccountName" . }}
      containers:
        - name: samba
          securityContext:
            capabilities:
              drop:
                - ALL
              add:
                - CHOWN
                - DAC_OVERRIDE
                - DAC_READ_SEARCH
                - FOWNER
                - NET_BIND_SERVICE
                - SETGID
                - SETUID
            readOnlyRootFilesystem: false
            runAsNonRoot: false
            runAsUser: 0
          env:
            - name: SSSD_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: sssdPassword
                  name: {{ include "licham-application.fullname" . }}-sssd
          image: "{{ .Values.networkShare.image.repository }}/{{ .Values.networkShare.image.sambaImage }}:{{ .Values.networkShare.image.tag }}"
          imagePullPolicy: {{ .Values.networkShare.image.pullPolicy }}
          ports:
            - name: samba
              containerPort: 445
              protocol: TCP
          livenessProbe:
            tcpSocket:
              port: 445
          readinessProbe:
            tcpSocket:
              port: 445
          startupProbe:
            tcpSocket:
              port: 445
          volumeMounts:
          {{- range .Values.persistentStorage }}
          {{- if .networkShare }}
            - name: {{ .name }}
              mountPath: /mnt/{{ .name }}
          {{- end }}
          {{- end }}
            - name: sssd-sockets
              mountPath: /var/lib/sss/pipes
        - name: sssd
          securityContext:
            capabilities:
              drop:
                - ALL
              add:
                - DAC_OVERRIDE
                - DAC_READ_SEARCH
            readOnlyRootFilesystem: false
            runAsNonRoot: false
            runAsUser: 0
          env:
            - name: SSSD_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: sssdPassword
                  name: {{ include "licham-application.fullname" . }}-sssd
          image: "{{ .Values.networkShare.image.repository }}/{{ .Values.networkShare.image.sssdImage }}:{{ .Values.networkShare.image.tag }}"
          imagePullPolicy: {{ .Values.networkShare.image.pullPolicy }}
          volumeMounts:
            - name: sssd-sockets
              mountPath: /var/lib/sss/pipes
      volumes:
        {{- range .Values.persistentStorage }}
        {{- if .networkShare }}
        - name: {{ .name }}
          persistentVolumeClaim:
            claimName: {{ include "licham-application.fullname" $ }}-{{ .name }}
        {{- end }}
        {{- end }}
        - name: sssd-sockets
          emptyDir: {}
      {{- with .Values.deployment.scheduling.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: topology.k8s.licham.io/spread
                operator: In
                values:
                - storage
            topologyKey: kubernetes.io/hostname
      {{- with .Values.deployment.scheduling.affinity }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.deployment.scheduling.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
{{- end }}
