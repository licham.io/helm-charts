---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "licham-game-server.fullname" . }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "licham-game-server.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.deployment.replicas }}
  selector:
    matchLabels:
      {{- include "licham-game-server.selectorLabels" . | nindent 6 }}
  strategy:
    type: {{ .Values.deployment.strategy }}
  template:
    metadata:
      {{- with .Values.deployment.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "licham-game-server.labels" . | nindent 8 }}
        {{- with .Values.deployment.podLabels }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      terminationGracePeriodSeconds: {{ .Values.deployment.terminationGracePeriod }}
      serviceAccountName: {{ include "licham-game-server.serviceAccountName" . }}
      {{- with .Values.deployment.podSecurityContext }}
      securityContext:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- if .Values.deployment.initContainer.enabled }}
      initContainers:
        - name: {{ .Chart.Name }}-steam-downloader
          {{- with .Values.deployment.securityContext }}
          securityContext:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.deployment.initContainer.command }}
          command:
            {{ . | toYaml | nindent 12 }}
          {{- end }}
          {{- with .Values.deployment.initContainer.env }}
          env:
            {{ . | toYaml | nindent 12 }}
          {{- end }}
          image: "{{ .Values.deployment.initContainer.image.repository }}/{{ .Values.deployment.initContainer.image.image }}:{{ .Values.deployment.initContainer.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
          {{- if or .Values.deployment.volumeMounts .Values.persistentStorage }}
          volumeMounts:
            {{- if .Values.deployment.volumeMounts }}
            {{ .Values.deployment.volumeMounts | toYaml | nindent 12 }}
            {{- end }}
            {{- range .Values.persistentStorage }}
            - name: {{ .name }}
              mountPath: {{ .mountPath }}
            {{- end }}
          {{- end }}
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          {{- with .Values.deployment.securityContext }}
          securityContext:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.deployment.command }}
          command:
            {{ . | toYaml | nindent 12 }}
          {{- end }}
          {{- if or .Values.deployment.env .Values.vaultStaticSecrets.env }}
          env:
            {{ .Values.deployment.env | toYaml | nindent 12 }}
            {{- range .Values.vaultStaticSecrets.env }}
            {{ $secretname := .name }}
            {{- range .vars }}
            - name: {{ . }}
              valueFrom:
                secretKeyRef:
                  key: {{ . }}
                  name: {{ include "licham-game-server.fullname" $ }}-{{ $secretname }}
            {{- end }}
            {{- end }}
          {{- end }}
          image: "{{ .Values.deployment.image.repository }}/{{ .Values.deployment.image.image }}:{{ .Values.deployment.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
          {{- if .Values.service.enabled }}
          ports:
            {{- range .Values.service.ports }}
            - name: {{ .name }}
              containerPort: {{ .targetPort }}
              protocol: {{ .protocol }}
            {{- end }}
          {{- end }}
          {{- if .Values.deployment.probes.enabled }}
          {{- with .Values.deployment.probes.livenessProbe }}
          livenessProbe:
            {{ . | toYaml | nindent 12 }}
          {{- end }}
          {{- with .Values.deployment.probes.readinessProbe }}
          readinessProbe:
            {{ . | toYaml | nindent 12 }}
          {{- end }}
          {{- if .Values.deployment.probes.auto.enabled }}
          livenessProbe:
            httpGet:
              path: {{ .Values.deployment.probes.auto.path }}
              port: {{ .Values.deployment.probes.auto.port }}
          readinessProbe:
            httpGet:
              path: {{ .Values.deployment.probes.auto.path }}
              port: {{ .Values.deployment.probes.auto.port }}
          {{- end }}
          {{- end }}
          resources:
            {{- toYaml .Values.deployment.resources | nindent 12 }}
          {{- if or .Values.deployment.volumeMounts .Values.vaultStaticSecrets.mount .Values.persistentStorage .Values.configMaps }}
          volumeMounts:
            {{- if .Values.deployment.volumeMounts }}
            {{ .Values.deployment.volumeMounts | toYaml | nindent 12 }}
            {{- end }}
            {{- range .Values.vaultStaticSecrets.mount }}
            - name: {{ .name }}
              mountPath: {{ .mountPath }}
              readOnly: true
            {{- end }}
            {{- range .Values.persistentStorage }}
            - name: {{ .name }}
              mountPath: {{ .mountPath }}
            {{- end }}
            {{- range .Values.configMaps }}
            - name: {{ .name }}
              mountPath: {{ .mountPath }}
              readOnly: true
            {{- end }}
          {{- end }}
      {{- if or .Values.deployment.volumes .Values.vaultStaticSecrets.mount .Values.persistentStorage .Values.configMaps }}
      volumes:
        {{- if .Values.deployment.volumes }}
        {{ .Values.deployment.volumes | toYaml | nindent 8 }}
        {{- end }}
        {{- range .Values.vaultStaticSecrets.mount }}
        - name: {{ .name }}
          secret:
            secretName: {{ include "licham-game-server.fullname" $ }}-{{ .name }}
        {{- end }}
        {{- range .Values.persistentStorage }}
        - name: {{ .name }}
          persistentVolumeClaim:
            claimName: {{ include "licham-game-server.fullname" $ }}-{{ .name }}
        {{- end }}
        {{- range .Values.configMaps }}
        - name: {{ .name }}
          configMap:
            name: {{ include "licham-game-server.fullname" $ }}-{{ .name }}
        {{- end }}
      {{- end }}
      {{- with .Values.deployment.scheduling.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.deployment.scheduling.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.deployment.scheduling.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
